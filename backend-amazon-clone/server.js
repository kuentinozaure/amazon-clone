const express = require("express");
const cors = require("cors");
const { v4: uuidV4 } = require("uuid");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const port = process.env.PORT || 3000;
const app = express();

app.set("", "");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// amazon-clone-api
// jLkSFHrlWHcuOUDW
// amazon-clone-db
const DATABASE_USER = "amazon-clone-api";
const DATABASE_PASWORD = "jLkSFHrlWHcuOUDW";
const DATABASE_NAME = "amazon-clone-db ";
const db = mongoose.connection;
const uri = `mongodb+srv://${DATABASE_USER}:${DATABASE_PASWORD}@cluster0.xya4n.mongodb.net/${DATABASE_NAME}`;

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

const apiKeySchema = mongoose.Schema({
  user_id: Number,
  creation_date: String,
  key: String,
  username: String,
  email: String,
});

const articleSchema = mongoose.Schema({
  title: String,
  description: String,
  price: Number,
  fast_delivery: Boolean,
  like: Number,
  images: [],
  brand: String,
  tags: [],
  categorie: String,
  size: String,
  quantity: String,
  posted_by: String,
});

const userSchema = mongoose.Schema({
  firstname: String,
  surname: String,
  mail: String,
  number: String,
  password: String,
});


const apiKeyTable = mongoose.model("apiKey", apiKeySchema, "apiKey");
const articleTable = mongoose.model("article", articleSchema, "article");
const userTable = mongoose.model("user", userSchema, "user");

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => {
  console.log("connected to mongo");
});

/**
 * Display a message on default route
 */
app.get("/", async (req, res) => {
  if (
    req.body === {} ||
    req.body.apikey === {} ||
    req.body.apikey === undefined
  ) {
    res.status(404).send({ error: `please provide your api key` });
  }
  data = await checkApiKeyUser(req.body.apikey);
  if (data) {
    res.json(`welcome back ${data.username}`);
  } else {
    res.json(`keep calm and contact admin for your api key`);
  }
});

/**
 * trending product -> search by like
 */
app.get("/trending", async (req, res) => {

  const articles = await articleTable
    .find({}, null, { sort: { like: "desc" } })
    .limit(5);

  try {
    res.json(articles);
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * find by categorie
 */
app.get("/categorie/:categorie", async (req, res) => {
  if (
    req.body === {} ||
    req.body.apikey === {} ||
    req.body.apikey === undefined
  ) {
    res.status(404).send({ error: `please provide your api key` });
  }

  if (req.params.categorie === "" || req.params.categorie === undefined) {
    res.status(404).send({ error: `please provide a categorie` });
  }

  data = await checkApiKeyUser(req.body.apikey);
  if (!data) {
    res.status(404).send({ error: `no api key provided` });
  }

  const articles = await articleTable
    .find({ categorie: { $regex: `.*${req.params.categorie}.*` } }, null, {})
    .limit(12);

  try {
    res.json(articles);
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * find by brand
 */
app.get("/brand/:brand", async (req, res) => {
  if (
    req.body === {} ||
    req.body.apikey === {} ||
    req.body.apikey === undefined
  ) {
    res.status(404).send({ error: `please provide your api key` });
  }

  if (req.params.brand === "" || req.params.brand === undefined) {
    res.status(404).send({ error: `please provide a brand` });
  }

  data = await checkApiKeyUser(req.body.apikey);
  if (!data) {
    res.status(404).send({ error: `no api key provided` });
  }

  const articles = await articleTable
    .find({ brand: { $regex: `.*${req.params.brand}.*` } }, null, {})
    .limit(12);

  try {
    res.json(articles);
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * find element posted by user
 */
app.get("/article/:userid", async (req, res) => {

  if (req.params.userid === "" || req.params.userid === undefined) {
    res.status(404).send({ error: `please provide a search terms` });
  }

  const articles = await articleTable
    .find({ posted_by: req.params.userid }, null, {})
    .limit(12);

  try {
    res.json(articles);
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * search element
 */
app.get("/search/:search", async (req, res) => {
  if (req.params.search === "" || req.params.search === undefined) {
    res.status(404).send({ error: `please provide a search terms` });
  }

  const articles = await articleTable
    .find(
      {
        title: { $regex: `.*${req.params.search}.*` },
      },
      null,
      {}
    )
    .limit(50);

  try {
    res.json(articles);
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * application  log in
 */
app.post("/login", async (req, res) => {
  if (
    req.body === {} ||
    req.body.apikey === {} ||
    req.body.apikey === undefined
  ) {
    res.status(404).send({ error: `please provide your api key` });
  }

  if (
    req.body === {} ||
    req.body.login === "" ||
    req.body.password === "" ||
    req.body.login === undefined ||
    req.body.password === undefined
  ) {
    res.status(404).send({ error: `Please provide your credentials` });
  }

  data = await checkApiKeyUser(req.body.apikey);
  if (!data) {
    res.status(404).send({ error: `no api key provided` });
  }

  const login = req.body.login;
  const password = req.body.password;
  const user = await userTable.find({ mail: login });

  if (user.length === 0 || user === undefined) {
    res.status(404).send({ error: `Your credentials is not good` });
  }

  console.log(require("crypto").createHash("sha256").update(password).digest("base64"))
  if (
    user[0].mail === login &&
    user[0].password ===
      require("crypto").createHash("sha256").update(password).digest("base64")
  ) {
    res.status(200).send({
      success: `welcome on the matrix ${user[0].firstname}`,
      user: user[0],
    });
  } else {
    res.status(404).send({ error: `Your credentials is not good` });
  }



});

/**
 * adding  article
 */
app.post("/article", async (req, res) => {
  if (
    req.body === {} ||
    req.body.apikey === {} ||
    req.body.apikey === undefined
  ) {
    res.status(404).send({ error: `please provide your api key` });
  }

  if (
    req.body === {} ||
    req.body.article === {} ||
    req.body.article === undefined
  ) {
    res.status(404).send({ error: `Please provide article to insert` }).end();
  }

  data = await checkApiKeyUser(req.body.apikey);
  if (!data) {
    res.status(404).send({ error: `no api key provided` });
  }

  articleB = req.body.article;

  const article = new articleTable(articleB);

  article.save((err, art) => {
    if (err)
      res.status(404).send({ error: `impossible to insert element` }).end();
      return res.json(art);
  });
});

/**
 * delete article
 */
app.delete("/article/:id", async (req, res) => {

  if (req.params.id === "" || req.params.id === undefined) {
    res.status(404).send({ error: `please provide a id` });
  }

  
  articleTable.findByIdAndDelete(req.params.id, (err, _) => {
    if (err) {
      res.send(err);
    } else {
      res.json(`delete successful`);
    }
  });
});

/**
 * check if user have his api key to acces of all feature of app
 * @param {*} apiKey this is the api key of the application
 */
async function checkApiKeyUser(apiKey) {
  const value = await apiKeyTable.findOne({
    key: apiKey,
  });

  if (value) {
    return value;
  } else {
    return null;
  }
}

app.listen(port);
