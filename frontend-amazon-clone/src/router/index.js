import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/feature/home/Home.vue'
import Article from '../views/feature/article/Article.vue'
import Search from '../views/feature/search/Search.vue'
import Account from '../views/feature/account/Account.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/article',
    name: 'Article',
    component: Article
  },
  {
    path: '/search',
    name: 'Search',
    component: Search
  },
  {
    path: '/account',
    name: 'Account',
    component: Account
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
