import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    connectedUser : {}
  },
  mutations: {
    setUser(state,user) {
      state.connectedUser = user
    }
  },
  actions: {
    setconnecteduser(context,user) {
      context.commit('setUser',user)
    }
  },
  getters: {
    getUser: state => {
      return state.user
    },
  },
  modules: {
  }
})
